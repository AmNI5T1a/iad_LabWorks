 
 # Contributing
  - **If you are interested in these labs or you just want to suggest your OWN solution, then:**
  - :one:. Fork the repository! [Click](https://www.earthdatascience.org/workshops/intro-version-control-git/about-forks/#:~:text=You%20can%20fork%20any%20repo,Source%3A%20GitHub%20Guides.)
  - :two:. Create your feature branch
  - :three:. Commit your changes
  - :four:. Push to the branch
  - :five:. Submit a pull request to this repository
  
    
# Dotnet
## First Laboratory work:
- [X] First task: 
    > **Description:**
    >
    > Here we need to create a `space`.
    >
    > For example : two-dimensional array, List(Vector2) or struct with two double or float points.
    >
    > _How to use my work as an example:_
    > -  Choose first task in main menu.
    > -  Input from keyboard number of points in your `space`.
    
    
- [X] Second task:
    > **Description:**
    >
    > Here you need to create a feature space, calculate the points inside it, merge neighboring points into one class/set 
    >
    > The next task is to calculate the intraset distance.
    >
    > Explanation: we multiply each point of set A to all other points of set A (the same set).
    >
    > Next, we are calculating distance between the sets, this is the same, but we multiply each point of the set A by each point of the set B.
    >
    > Use all formulas from here: [Интеллектуальный анализ данных - 1.pptx](https://drive.google.com/drive/folders/1A4qAGeKilR3Sfo9bIo6zLJrC8F-faaCA)
    >
    > _How to use my work as an example:_
    > -  Choose second task in main menu.
    > -  Inject number of classes.
- [X] Third task: 
    > **Description:** 
    >
    > You need to create space, fill it with x and y axis, calculate the Eulidean distance between all points and choose those that are closest 
    >
    > _How to use my work as an example:_
    > -  Choose third task in main menu.
    > -  Input number of neighbors you want to find. 
    > -  Input from keyboard number of points in your `space`.
    > -  Choose any point from the proposed.
## Second Laboratory work:
- [X] Main task: 
    > **Description:** 
    >
    > Our task is to create a multispectral classifier based on models and standards
    >
    > Recommend to read main.pdf in lab2 library
    >[Сlick](https://drive.google.com/drive/folders/1A4qAGeKilR3Sfo9bIo6zLJrC8F-faaCA)
_____________________________________________________________________________________________________________________________________
# Python             
               
               
               

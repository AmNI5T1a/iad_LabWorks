﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TCFI
{

    public partial class TCFI : Form
    {
        public TCFI()
        {
            InitializeComponent();
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openImage.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = new Bitmap(openImage.FileName);
            }
        }

        #region CreateAndPaintRectangleOnPictureBox
        private Point RectStartPoint;

        // Start Rectangle
        //
        private void pictureBox1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            // Determine the initial rectangle coordinates...
            RectStartPoint = e.Location;
            Invalidate();
        }

        private Rectangle Rect = new Rectangle();

        // Draw Rectangle
        private void pictureBox1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;
            Point tempEndPoint = e.Location;

            Rect.Location = new Point(
                Math.Min(RectStartPoint.X, tempEndPoint.X),
                Math.Min(RectStartPoint.Y, tempEndPoint.Y));
            Rect.Size = new Size(
                Math.Abs(RectStartPoint.X - tempEndPoint.X),
                Math.Abs(RectStartPoint.Y - tempEndPoint.Y));

            label9.Text = Rect.Location.X.ToString();
            label10.Text = Rect.Location.Y.ToString();
            label11.Text = (Rect.Location.X + Rect.Size.Width).ToString();
            label12.Text = (Rect.Location.Y + Rect.Size.Height).ToString();

            pictureBox1.Invalidate();

        }

        /// <summary>
        /// Draw Area on PictureBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            Brush selectionBrush = new SolidBrush(Color.FromArgb(200, 200, 200, 220));

            if (pictureBox1.Image != null)
            {
                if (Rect != null && Rect.Width > 0 && Rect.Height > 0)
                {
                    e.Graphics.FillRectangle(selectionBrush, Rect);
                }
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (Rect.Contains(e.Location))
                {
                    Debug.WriteLine("Right click");
                }
            }
        }
        #endregion

        private void GetInformationAboutPicture(object sender, EventArgs e)
        {
            if (openImage.FileName == null || openImage.FileName == "")
                return;

            Bitmap image = new Bitmap(openImage.FileName);

            GetPixelsCount(ref image);
            GetSizeOfImage(ref image);
        }

        private void GetPixelsCount(ref Bitmap image)
        {
            textBox2.Text = (image.Width * image.Height).ToString();
        }
        private void GetSizeOfImage(ref Bitmap image)
        {
            textBox3.Text = image.Size.Width.ToString();
            textBox4.Text = image.Size.Height.ToString();
        }

        private void ClearPictureBox(object sender, EventArgs e)
        {
            if (pictureBox1.Image == null)
                return;

            pictureBox1.Image = null;
            textBox2.Text = null;
            textBox3.Text = null;
            textBox4.Text = null;
            listBox1.Items.Clear();
            allCreatedFields.Clear();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BtnModel_Click(object sender, EventArgs e)
        {
            if (tbModel != null && tbModel.Text != "")
            {
                lbModel.Items.Add(tbModel.Text);
            }
            tbModel.Clear();
        }
        private void TbModel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (tbModel != null && tbModel.Text != "")
                {
                    lbModel.Items.Add(tbModel.Text);
                }
                tbModel.Clear();
            }
        }

        private void BtnEtalon_Click(object sender, EventArgs e)
        {
            if (tbEtalon != null && tbEtalon.Text != "")
            {
                cbEtalon.Items.Add(tbEtalon.Text);
            }
            tbEtalon.Clear();
        }

        private void TbEtalon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (tbEtalon != null && tbEtalon.Text != "")
                {
                    cbEtalon.Items.Add(tbEtalon.Text);
                }
                tbEtalon.Clear();
            }
        }

        private List<AreaModel> allCreatedFields = new List<AreaModel>();

        private void BtnCreateModel_Click(object sender, EventArgs e)
        {
            if (lbModel.SelectedItem != null && cbEtalon.SelectedItem != null)
            {
                var tempModel = new AreaModel(lbModel.SelectedItem.ToString(), cbEtalon.SelectedItem.ToString(), Rect);
                //listBox1.Items.Add($"{Rect.Location},{Rect.Location.X +Rect.Size.Width}, {Rect.Location.Y + Rect.Size.Height}");
                listBox1.Items.Add($"Created new {lbModel.SelectedItem} with: X:({Rect.Location.X}-{Rect.Location.X + Rect.Size.Width}) Y:({Rect.Location.Y}-{Rect.Location.Y + Rect.Size.Height})");
                listBox1.Items.Add($"Etalon:{cbEtalon.SelectedItem}");
                allCreatedFields.Add(tempModel);
            }
            else
            {
                Debug.WriteLine("Model or etalon doesn't selected in GUI");
            }

        }
        

        private void GenerateNewColors(object sender, EventArgs e)
        {
            PictureGenerator generator = new PictureGenerator();

            Bitmap newImage = generator.GenerateNewPictureBasedOnModels(new Bitmap(pictureBox1.Image), allCreatedFields);
            pictureBox1.Image = newImage;
        }
    }
}

//    public abstract class RenderPage
//    {
//        protected byte counter;

//        protected static byte counterr;
//        public abstract void SetCounterVariable();
//    }
//}

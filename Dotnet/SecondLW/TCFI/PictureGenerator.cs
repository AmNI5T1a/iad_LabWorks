﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCFI
{
    class PictureGenerator
    {
        struct XYAxis
        {
            public List<Int32> listXAxis{ get;private set; }
            public List<Int32> listYAxis { get;private set; }

            public XYAxis(List<Int32> x, List<Int32> y)
            {
                this.listXAxis = x;
                this.listYAxis = y;
            }
            public void AddNewXY(int x,int y)
            {
                listXAxis.Add(x);
                listYAxis.Add(y);
            }
        }

        private Bitmap _imageForProcessing;

        private List<AreaModel> _listOfAreaModels = new List<AreaModel>();

        private XYAxis listOfXY = new XYAxis(new List<int>(), new List<int>());

        public Bitmap GenerateNewPictureBasedOnModels(Bitmap image, List<AreaModel> listOfCreatedFields)
        {
            this._imageForProcessing = image;
            this._listOfAreaModels = listOfCreatedFields;

            FindAndSetModelsColor();
            DetermineAreaForRemainingPoints();

            return image;
        }

        private void FindAndSetModelsColor()
        {

            for (byte v = 0; v < _listOfAreaModels.Count; v++)
            {
                Color generalColorOfArea = _listOfAreaModels[v].FindGeneralColor(_imageForProcessing);

                for (int d = _listOfAreaModels[v].fieldOfModel.Location.X; d < (_listOfAreaModels[v].fieldOfModel.Location.X + _listOfAreaModels[v].fieldOfModel.Size.Width); d++)
                {
                    for (int b = _listOfAreaModels[v].fieldOfModel.Location.Y; b < (_listOfAreaModels[v].fieldOfModel.Location.Y + _listOfAreaModels[v].fieldOfModel.Size.Height); b++)
                    {
                        _imageForProcessing.SetPixel(d, b, generalColorOfArea);

                        listOfXY.AddNewXY(d, b);
                    }
                }
            }
        }

        private void DetermineAreaForRemainingPoints()
        {
            for (ushort x = 0; x < _imageForProcessing.Width; x++)
            {
                for (ushort y = 0; y < _imageForProcessing.Height; y++)
                {
                    if (!(listOfXY.listXAxis.Contains(x) && listOfXY.listYAxis.Contains(y)))
                    {
                        Color colourOfThisPixel = _imageForProcessing.GetPixel(x, y);
                        Color newColorToThisPixel = DetermineColorForPoint(colour: ref colourOfThisPixel, _listOfAreaModels);

                        _imageForProcessing.SetPixel(x, y, newColorToThisPixel);
                    }
                }
            }
        }

        private Color DetermineColorForPoint(ref Color colour, List<AreaModel> allCreatedFields)
        {
            Color currentNearestColor = allCreatedFields[0].generalColor;


            for (byte v = 1; v < allCreatedFields.Count; v++)
            {
                byte status = 0;
                if (Math.Abs(colour.A - allCreatedFields[v].generalColor.A) < Math.Abs(colour.A - currentNearestColor.A))
                {
                    status++;
                }
                if (Math.Abs(colour.G - allCreatedFields[v].generalColor.G) < Math.Abs(colour.G - currentNearestColor.G))
                {
                    status++;
                }
                if (Math.Abs(colour.B - allCreatedFields[v].generalColor.B) < Math.Abs(colour.B - currentNearestColor.B))
                {
                    status++;
                }
                if (status >= 2)
                {
                    currentNearestColor = allCreatedFields[v].generalColor;
                }
            }

            return currentNearestColor;
        }
    }
}

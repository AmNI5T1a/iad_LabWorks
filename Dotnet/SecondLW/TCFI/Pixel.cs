﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCFI
{
    class Pixel
    {
        public int xAxis { get; private set; }
        public int yAxis { get; private set; }

        public static List<Pixel> listOfPixels = new List<Pixel>();

        public Pixel(int x, int y)
        {
            this.xAxis = x;
            this.yAxis = y;
        }

        public bool CheckValidOfPoints(in int x, in int y)
        {
            if ((x == xAxis) && (y == yAxis))
                return true;
            else
                return false;
        }
    }
}

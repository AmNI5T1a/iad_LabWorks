﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCFI
{
    class AreaModel
    {
        public string model { get; private set; }
        public string etalon { get; private set; }

        public Color generalColor { get; private set; }

        public Rectangle fieldOfModel { get; private set; }

        public AreaModel(string model, string etalon, Rectangle rect)
        {
            this.model = model;
            this.etalon = etalon;
            this.fieldOfModel = rect;
        }

        public Color FindGeneralColor(Bitmap image)
        {
            List<Color> listOfColors = new List<Color>();


            for (int v = fieldOfModel.Location.X; v < (fieldOfModel.Location.X + fieldOfModel.Size.Width); v++)
            {
                for (int b = fieldOfModel.Location.Y; b < (fieldOfModel.Location.Y + fieldOfModel.Size.Height); b++)
                {
                    if (!listOfColors.Contains(image.GetPixel(v, b)))
                    {
                        listOfColors.Add(image.GetPixel(v, b));
                    }
                }
            }

            Color generalColor = listOfColors[0];

            for (uint v = 1; v < listOfColors.Count; v++)
            {
                generalColor = Color.FromArgb((generalColor.R + listOfColors[(int)v].R) / 2, (generalColor.G + listOfColors[(int)v].G) / 2, (generalColor.B + listOfColors[(int)v].B) / 2);
            }
            this.generalColor = generalColor;
            return generalColor;
        }
        public void PaintInGeneralColor(Bitmap image)
        {
            for (long v = fieldOfModel.Location.X; v < (fieldOfModel.Location.X + fieldOfModel.Size.Width); v++)
            {
                for (long b = fieldOfModel.Location.Y; b < (fieldOfModel.Location.Y + fieldOfModel.Size.Height); b++)
                {
                    image.SetPixel((int)v, (int)b, generalColor);
                }
            }
        }
    }
}
